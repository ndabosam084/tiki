/**
 * Support JavaScript for FullCalendar Resource Views used by wikiplugin_trackercalendar
 */

import Calendar from "@event-calendar/core";
import listPlugin from "@event-calendar/list";
import dayGridPlugin from "@event-calendar/day-grid";
import resourceTimelinePlugin from "@event-calendar/resource-timeline";
import resourceTimeGridPlugin from "@event-calendar/resource-time-grid";
import moment from "moment";

$.fn.setupFullCalendar = function (tcPluginParams) {
    this.each(function () {
        let cal = this;

        let storeEvent = function (eventInfo) {
            let event = eventInfo.event,
                end = event.end,
                start = event.start,
                request = {
                    itemId: event.id,
                    trackerId: tcPluginParams.trackerId,
                    ajax: true,
                };

            if (!end) {
                end = start;
            }

            request["fields~" + tcPluginParams.begin] = moment(start).unix() + start.getTimezoneOffset() * 60;
            request["fields~" + tcPluginParams.end] = moment(end).unix() + end.getTimezoneOffset() * 60;

            let resource = event.getResources();
            if (resource.length) {
                resource = resource[0];
            }
            request["fields~" + tcPluginParams.resource] = resource.title;

            $.post($.service("tracker", "update_item"), request, null, "json");
        };

        let slotLabelTimeFormat = {
            hour: "numeric",
            minute: "2-digit",
            meridiem: tcPluginParams.timeFormat,
            hour12: tcPluginParams.timeFormat,
        };

        var calendar = new Calendar({
            target: cal,
            props: {
                plugins: [listPlugin, resourceTimelinePlugin, resourceTimeGridPlugin, dayGridPlugin],
                options: {
                    date: tcPluginParams.initialDate,
                    eventTimeFormat: {
                        hour: "numeric",
                        minute: "2-digit",
                        meridiem: tcPluginParams.timeFormat,
                        hour12: tcPluginParams.timeFormat,
                    },
                    views: {
                        timeGrid: {
                            // options apply to timeGridWeek and timeGridDay views
                            slotLabelFormat: slotLabelTimeFormat,
                        },
                        resourceTimelineDay: {
                            // options apply to timeGridWeek and timeGridDay views
                            slotLabelFormat: slotLabelTimeFormat,
                        },
                        resourceTimelineWeek: {
                            //slotLabelInterval: "24:00:00",
                            slotDuration: "06:00:00",
                            slotLabelFormat: [
                                // top level of text
                                {
                                    month: "long",
                                    year: "numeric",
                                },
                                // lower level of text
                                {
                                    day: "numeric",
                                    weekday: "short",
                                },
                            ],
                        },
                    },
                    viewClassNames: function (currentView) {
                        $(cal).tikiModal();
                        console.debug(currentView.view.type); // useful for debugging
                    },
                    headerToolbar: {
                        start: "prev,next today",
                        center: "title",
                        end: tcPluginParams.views,
                    },
                    editable: true,
                    eventSources: [
                        {
                            url: $.service(
                                "tracker_calendar",
                                "list",
                                $.extend(tcPluginParams.filterValues, {
                                    title: tcPluginParams.title,
                                    description: tcPluginParams.description,
                                    trackerId: tcPluginParams.trackerId,
                                    colormap: tcPluginParams.colormap,
                                    beginField: tcPluginParams.begin,
                                    endField: tcPluginParams.end,
                                    resourceField: tcPluginParams.resource,
                                    coloringField: tcPluginParams.coloring,
                                    filters: tcPluginParams.body,
                                    maxRecords: tcPluginParams.maxEvents,
                                })
                            ),
                        },
                    ],
                    buttonText: {
                        dayGridMonth: tr("month"),
                        resourceTimelineDay: tcPluginParams.labelResDay,
                        resourceTimelineWeek: tcPluginParams.labelResWeek,
                        resourceTimelineMonth: tcPluginParams.labelResMonth,
                        resourceTimelineYear: tcPluginParams.labelResYear,
                        listDay: tcPluginParams.labelListDay,
                        listWeek: tcPluginParams.labelListWeek,
                        listMonth: tcPluginParams.labelListMonth,
                        listYear: tcPluginParams.labelListYear,
                        today: tcPluginParams.labelToday,
                        resourceTimeGridWeek: tcPluginParams.labelAgendaWeek,
                        resourceTimeGridDay: tcPluginParams.labelAgendaDay,
                    },
                    resources: tcPluginParams.resourceList,
                    allDayText: tcPluginParams.labelAllDay,
                    firstDay: tcPluginParams.firstDayofWeek,
                    slotDuration: tcPluginParams.slotDuration,
                    slotMinTime: tcPluginParams.minHourOfDay,
                    slotMaxTime: tcPluginParams.maxHourOfDay,
                    view: tcPluginParams.dView,
                    nowIndicator: true,
                    eventClick: function (eventData) {
                        eventData.jsEvent.preventDefault();
                        if (tcPluginParams.url) {
                            const event = eventData.event;
                            let actualURL = tcPluginParams.url;
                            actualURL += actualURL.indexOf("?") === -1 ? "?" : "&";

                            if (tcPluginParams.trkitemid === "y" && tcPluginParams.addAllFields === "n") {
                                // "simple" mode
                                actualURL += "itemId=" + event.id;
                            } else {
                                let lOp = "";
                                let html = $.parseHTML(event.description) || [];

                                // Store useful data values to the URL for Wiki Argument Variable
                                // use and to javascript session storage for JQuery use
                                actualURL += "trackerid=" + event.trackerId;
                                if (event.trkitemid === "y") {
                                    actualURL = actualURL + "&itemId=" + event.id;
                                } else {
                                    actualURL = actualURL + "&itemid=" + event.id;
                                }
                                actualURL = actualURL + "&title=" + event.title;
                                actualURL = actualURL + "&end=" + event.end;
                                actualURL = actualURL + "&start=" + event.start;
                                if (tcPluginParams.useSessionStorage) {
                                    sessionStorage.setItem("trackerid", event.trackerId);
                                    sessionStorage.setItem("title", event.title);
                                    sessionStorage.setItem("start", event.start);
                                    sessionStorage.setItem("itemid", event.id);
                                    sessionStorage.setItem("end", event.end);
                                    sessionStorage.setItem("eventColor", event.color);
                                }

                                // Capture the description HTML as variables
                                // with the label being the variable name
                                $.each(html, function (i, el) {
                                    if (isEven(i) == true) {
                                        lOp = el.textContent.replace(" ", "_");
                                    } else {
                                        actualURL = actualURL + "&" + lOp + "=" + el.textContent;
                                        if (tcPluginParams.useSessionStorage) {
                                            sessionStorage.setItem(lOp, el.textContent);
                                        }
                                    }
                                });
                            }

                            location.href = actualURL;
                            return false;
                        } else {
                            // standard tracker item view/edit
                            let e = eventData.event;

                            if (e.startEditable && e.extendedProps.trackerId) {
                                var info = {
                                    trackerId: e.extendedProps.trackerId,
                                    itemId: e.id,
                                };
                                $.openModal({
                                    remote: $.service("tracker", "update_item", info),
                                    size: "modal-lg",
                                    title: e.title,
                                    open: function () {
                                        $("form:not(.no-ajax)", this)
                                            .addClass("no-ajax") // Remove default ajax handling, we replace it
                                            .on(
                                                "submit",
                                                ajaxSubmitEventHandler(function (data) {
                                                    $(this).parents(".modal").modal("hide");
                                                    calendar.refetchEvents();
                                                })
                                            );
                                    },
                                });
                                return false;
                            } else {
                                return true;
                            }
                        }
                    },
                    loading: function (isLoading) {
                        if (isLoading) {
                            $(cal).tikiModal(tr("Loading..."));
                        } else {
                            $(cal).tikiModal(tr(""));
                        }
                    },
                    eventDidMount: function (arg) {
                        let event = arg.event;
                        let element = $(arg.el);
                        element.attr("title", event.title);
                        element.popover({
                            trigger: "hover",
                            html: true,
                            content: event.extendedProps.description,
                            container: "body",
                            delay: { show: 250, hide: 500 },
                            customClass: "popover-sm",
                        });
                    },
                    dateClick: function (date) {
                        if (tcPluginParams.canInsert) {
                            var info = {
                                trackerId: tcPluginParams.trackerId,
                            };

                            let momentDate = moment(date.date);
                            info[tcPluginParams.beginFieldName] = momentDate.unix();
                            info[tcPluginParams.endFieldName] = momentDate.add(1, "h").unix();
                            if (tcPluginParams.url) {
                                $('<a href="#"/>').attr("href", tcPluginParams.url);
                            } else {
                                $.openModal({
                                    remote: $.service("tracker", "insert_item", info),
                                    size: "modal-lg",
                                    title: tcPluginParams.addTitle,
                                    open: function () {
                                        $("form:not(.no-ajax)", this)
                                            .addClass("no-ajax") // Remove default ajax handling, we replace it
                                            .on(
                                                "submit",
                                                ajaxSubmitEventHandler(function (data) {
                                                    $(this).parents(".modal").modal("hide");
                                                    calendar.refetchEvents();
                                                })
                                            );
                                    },
                                });
                            }
                        }
                        return false;
                    },
                    eventResize: storeEvent,
                    eventDrop: storeEvent,
                    height: "auto",
                    dayMinWidth: 150, // will cause horizontal scrollbars
                },
            },
        });

        if (jqueryTiki.print_pdf_from_url !== "none") {
            $(function () {
                $("#" + tcPluginParams.id).addFullCalendarPrint("#calendar-pdf-btn", calendar);
            });
        }
    });
};
